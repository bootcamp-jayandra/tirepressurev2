package asset;

public class Appearance{
	private static final String[] programChoice = new String[] {
		"The application will be closed", "2-front and 2-rear",
		"2-front and 4-rear", "4-front and 4-rear"};
	
	private static void showAppDescription(){
		clearTerminalDisplay();
		System.out.printf("================================================\n");
		System.out.printf("|                                              |\n");
		System.out.printf("|     Welcome to the Pressure Reader App!      |\n");
		System.out.printf("|                                              |\n");
		System.out.printf("| Use this app to check each of your tire      |\n");
		System.out.printf("| pressure!                                    |\n");
		System.out.printf("|                                              |\n");
	}
	
	public static void showInitialOption(){
		showAppDescription();
		System.out.printf("| Choose how many tire do you have on your car |\n");
		System.out.printf("| (1) 2-front and 2-rear                       |\n");
		System.out.printf("| (2) 2-front and 4-rear                       |\n");
		System.out.printf("| (3) 4-front and 4-rear                       |\n");
		System.out.printf("| (0) Close the program                        |\n");
		showBottomBorder();
	}
	
	public static void showChoosenOption(int choice){
		showAppDescription();
		System.out.printf("| You chose number %d option!                   |\n", choice);
		
		switch(choice){
			case 0:
				System.out.printf("| The application will be closed.              |\n");
				System.out.printf("| Thank you for using our application!         |\n");
				showBottomBorder();
				System.exit(0);
			default:
				System.out.printf("| Please insert your %s detail |\n", programChoice[choice]);
				showBottomBorder();
				break;
		}
	}
	
	public static void showResult(boolean isPressureGood){
		showAppDescription();
		if(isPressureGood){
			System.out.printf("|          ;;YOUR CAR IS GOOD TO USE;;         |\n");
		}
		else {
			System.out.printf("|          ;;YOUR CAR WHEEL IS BAD!!;;         |\n");
		}
		System.out.printf("|                                              |\n");
		System.out.printf("| Do you want to start it from zero? (Y/N)     |\n");
		showBottomBorder();
	}
	
	private static void showBottomBorder(){
		System.out.printf("|                                              |\n");
		System.out.printf("================================================\n");
	}
		
	private static void clearTerminalDisplay(){
		try {
			if (System.getProperty("os.name").contains("Windows")){
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			}
			else {
				Runtime.getRuntime().exec("clear");
			}
		}
		catch (Exception exception){
			System.out.printf("Sorry, you're currently using unknown OS!");
		}
	}
}