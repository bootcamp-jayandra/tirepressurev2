import asset.Appearance;
import tire.Tire;
import util.*;

public class Main{
	private static Input input = new Input();
	
	public static void main(String[] args){
		do {
			Tire tire = new Tire();
			Appearance.showInitialOption();
			input.inputTireOption();
			Appearance.showChoosenOption(input.getTireOption());
			tire.inputPressure(input.getTireOption());
			Appearance.showResult(tire.isGoodPressure());
			input.inputRestartOption();
		}
		while (input.getRestartOption().equalsIgnoreCase("Y"));
		
		Appearance.showChoosenOption(0);
	}
}