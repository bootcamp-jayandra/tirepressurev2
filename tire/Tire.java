package tire;

import util.InputValidator;
import java.util.Scanner;

public class Tire extends InputValidator{
	private static final Scanner scan = new Scanner(System.in);
	
	private static final String[] tireRow = {"FRONT", "REAR", "2nd REAR", "2nd FRONT"},
						   tireColumn = {"RIGHT", "LEFT"};
	private static final int[] minRowTire = {32, 35, 34, 37},
						maxRowTire = {42, 45, 44, 47},
						differenceColumnTire = {3, 5, 5, 3};
	private boolean goodPressure = true;
	
	public boolean isGoodPressure(){
		return this.goodPressure;
	}
	
	public void inputPressure(int tireRowAvailable){
		int[] rowPressureTemp = new int[tireColumn.length];
		for(int row=0; row<=tireRowAvailable; row++){
			for (int column=0; column<tireColumn.length; column++){
				System.out.printf("\nInput your %s %s TIRE pressure: ", tireRow[row], tireColumn[column]);
				rowPressureTemp[column] = super.safeInteger();
				this.goodPressure &= isPressureInRange(
					rowPressureTemp[column], row, column);
				System.out.println(this.goodPressure);
			}
			this.goodPressure &= isPressureStable(rowPressureTemp, row);
		}
		System.out.printf("\n\n;;PRESS [ENTER] TO SHOW THE FINAL RESULT;;");
		scan.nextLine();
	}
	
	private static boolean isPressureInRange(int tirePressure, int row, int column){
		if (tirePressure<minRowTire[row] || tirePressure>maxRowTire[row]){
			System.out.printf("WARNING: %s %s TIRE pressure is out of limit! Should be in range %d - %d psi\n",
				tireRow[row], tireColumn[column], minRowTire[row], maxRowTire[row]);
			return false;
		}
		return true;
	}
	
	private static boolean isPressureStable(int[] rowPressure, int row){
		if (Math.abs(rowPressure[0]-rowPressure[1]) > differenceColumnTire[row]){
			System.out.printf("WARNING: %s TIRE pressure is unstable! Pressure difference should be <%d psi\n",
				tireRow[row], differenceColumnTire[row]);
			return false;
		}
		return true;
	}

}