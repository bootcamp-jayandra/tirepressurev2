package util;

import java.util.Scanner;

public class InputValidator{
	private static Scanner scan = new Scanner(System.in);
							   
	protected int safeInteger(){
		while (true){
			try {
				return scan.nextInt();
			}
			catch (Exception e){
				System.out.printf("\nEnter the correct integer: ");
				scan.nextLine();
			}
		}
	}
}