package util;

import java.util.Scanner;

public class Input extends InputValidator{
	private static Scanner scan = new Scanner(System.in);
	private static InputValidator input = new InputValidator();
	
	private int tireOption;
	private String restartOption;
	
	public void inputTireOption(){
		do {
			System.out.printf("\nYour tire choice: ");
			this.tireOption = input.safeInteger();
			if (this.tireOption < 0 || this.tireOption > 3){
				System.out.printf("Input the proper option number!\n");
			}
		}
		while (this.tireOption < 0 || this.tireOption > 3);
	}
	
	public void inputRestartOption(){
		do {
			System.out.printf("\nYour answer: ");
			this.restartOption = scan.nextLine();
			if (!(this.restartOption.equalsIgnoreCase("Y") || this.restartOption.equalsIgnoreCase("N"))){
				System.out.printf("Input the proper answer!\n");
			}
		}
		while (!(this.restartOption.equalsIgnoreCase("Y") || this.restartOption.equalsIgnoreCase("N")));
	}
	
	public int getTireOption(){
		return this.tireOption;
	}
	
	public String getRestartOption(){
		return this.restartOption;
	}
}